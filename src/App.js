import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './component/Header/index'
import MaterialTableDemo from './container/Table';

function App() {
  return (
    <div>
      <Header/>
      <MaterialTableDemo />
    </div>
  );
}

export default App;
