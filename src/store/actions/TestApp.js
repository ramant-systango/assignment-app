import API  from '../../service/api'
export const SET_DATA = 'SET_DATA'
export const FETCH_DATA = 'FETCH_DATA'
export const FETCH_DATA_FAILED = 'FETCH_DATA_FAILED'
export const FETCH_DATA_SUCCESS = 'FETCH_DATA_SUCCESS'

export const setData = ( data ) => {
    return {
        type: SET_DATA,
        data: data,
    };
};

export const fetchDataSuccess = (data, newPage) => {
    return {
        type: FETCH_DATA_SUCCESS,
        data : data,
        page : newPage
    }
}

export const fetchDataFailed = () => {
    return {
        type: FETCH_DATA_FAILED
    };
};


export const initData = () => {
    return dispatch => {
        API.get( '/products?page=1&limit=10' )
            .then( response => {               
               dispatch(setData(response.data));
            } )
            .catch( error => {
                dispatch(fetchDataFailed());
            } );
    };
};

export const fetchData = (page, newPage) => {
    return dispatch => {
        const queryParams = '?limit=10&page=' + page ;        
        API.get( '/products' + queryParams )
        .then( response => {                          
           dispatch(fetchDataSuccess(response.data, newPage));
        } )
        .catch( error => {
            dispatch(fetchDataFailed());
        } );
    }    
} 