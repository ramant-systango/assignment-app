import { updateObject } from '../../utility/helper';
import { 
    SET_DATA,
    FETCH_DATA_FAILED,
    FETCH_DATA,
    FETCH_DATA_SUCCESS
    
} from '../actions/TestApp';

const initialState = {
    data: [],
    page : 0,
    rowsPerPage: 10,
    totalRow: 50,
    pageNumber: 1,
    error : false
}

const setData = (state, action) => {
    return updateObject( state, {
        data : action.data,
        error: false,    
    } );
};

const fetchData = (state, action) => {
    return updateObject( state, {
        data: action.data,
    })
}

const fetchDataSuccess = ( state, action) => {
    return updateObject( state, {
        data : action.data,
        page: action.page
    })
}

const fetchDataFailed = (state, action) => {
    return updateObject( state, { error: true } );
};

const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case SET_DATA: return setData(state, action);   
        case FETCH_DATA: return fetchData(state, action); 
        case FETCH_DATA_FAILED: return fetchDataFailed(state, action);
        case FETCH_DATA_SUCCESS: return fetchDataSuccess(state, action);
        default: return state;
    }
};

export default reducer;