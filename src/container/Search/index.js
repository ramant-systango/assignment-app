import React, { useState } from 'react';
import { InputGroup, FormControl } from 'react-bootstrap';
import { useSelector } from 'react-redux';

const Search = props => {

    const [ searchInput, setInput ] = useState("");

    const data = useSelector( state => state.data )
    
    const handleChange = event => {
        setInput(event.target.value);
    };
        
    const globalSearch = () => {
        let filteredData = [];
        data.map(value => {    
            if( ( value.name.toLowerCase().includes(searchInput.toLowerCase()) ||
                value.price.toString().toLowerCase().includes(searchInput.toLowerCase()) ||
                value.id.toString().toLowerCase().includes(searchInput.toLowerCase()) ) && 
                searchInput !== '') 
            {
              filteredData.push(value);
            }
            return 0;
        });    
        return filteredData;
    }
   
    return (
        <div className="container mt-5">
            <div className='row'>
                <div className="col-sm-4">
                    <InputGroup>
                        <FormControl
                            value={searchInput}
                            onChange={handleChange}
                            placeholder="Search..."
                        />
                    </InputGroup>              
                </div>                        
            </div>
        </div>
    )
}

export default Search;