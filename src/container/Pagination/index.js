import React, { useState } from 'react';
import { Pagination } from 'react-bootstrap'

import axios from 'axios';

const PaginationServerSide = props => {

    const [ disableNext, setDisableNext ] = useState(false);
    const [ disablePrev, setDisablePrev ] = useState(true);
    const [ pages, setPage ] = useState(1);
    const [ pageNo, setPageNo ] = useState(1)

    
    const paginate = pageNumber => {
        setPage(pageNumber)
        
        if (pageNumber>4 ) {
            setDisableNext(true); setDisablePrev(false);
        }
        else if (pageNumber<2 ) {
            setDisableNext(false); setDisablePrev(true);
        }
        else {
            setDisableNext(false); setDisablePrev(false);
        }   
  
        axios.get(`https://5e904a9a2810f4001648ad5e.mockapi.io/api/v1/products?page=${pageNumber}&limit=10`)
            .then(response => {
                this.setState({
                    data:response.data,
                    pages : pageNumber
                });
            })
  
    }

    const paginateNext = () => {
        let {pages} = this.state;  
        
        if (pages>4){
            setDisableNext(true); setDisablePrev(false);
        }
        else {
            setDisableNext(false); setDisablePrev(false);
        }
        axios.get(`https://5e904a9a2810f4001648ad5e.mockapi.io/api/v1/products?page=${pages+1}&limit=10`)
            .then(response => {
                this.setState({
                    data:response.data,
                    pages:pages+1
                });
            })
    }

    const paginatePrevious = () => {
        let {pages} = this.state; 
        
        if (pages<2){
            setDisableNext(false); setDisablePrev(true);
          this.setState({disableNext:false, disablePrev: true })
        }
        axios.get(`https://5e904a9a2810f4001648ad5e.mockapi.io/api/v1/products?page=${pages-1}&limit=10`)
            .then(response => {
                this.setState({
                    data:response.data,
                    pages:pages-1
                });
            })
    }

    let items = [];
    for (let number = 1; number <= 5; number++) {
        items.push(
        <Pagination.Item key={number} onClick={()=> paginate(number)}>
            {number}
        </Pagination.Item>,
        );
    }

    return (
            
        <Pagination>  
            <Pagination.First disabled={disablePrev} onClick={paginatePrevious}/>
                <Pagination >{items}</Pagination>
            <Pagination.Last disabled={disableNext} onClick={paginateNext}/>
        </Pagination>

    )
}
export default PaginationServerSide;