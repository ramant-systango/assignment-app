import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as actions from '../../store/actions/TestApp';
import MaterialTable from 'material-table';
import TablePagination from '@material-ui/core/TablePagination';
import ShowModal from '../../component/Modal';

export default function MaterialTableDemo() {
    const [st] = useState({
        columns: [
            { title : 'ID', field: 'id'},
            { title: 'Name', field: 'name' },
            { title: 'Price', field: 'price'}
        ],
        data: []        
    });

    const [ show, setShow ] = useState(false);
    const [ detail, setDetail ] = useState({});
    
    const state = useSelector(s => {
       return { data : s.data,
                rowsPerPage : s.rowsPerPage,
                totalRow: s.totalRow,
                pageNumber: s.pageNumber,
                page: s.page
        }
    })
  
    const dispatch = useDispatch();
    const onInitData = useCallback(
        () => dispatch(actions.initData()),
        [dispatch]
    );
    const onFetchData = useCallback(
        (page, newPage) => dispatch(actions.fetchData( page, newPage)),
        [dispatch]
    );

    useEffect(
        () => onInitData(),
        [onInitData]
    );
    
    const closeHandler = () => {
        setShow(false);
    }
    const handleRowClick = (event, rowData) => {
        setDetail(rowData);
        setShow(true);
    };

    const handleChangePage = (newPage) => {
        const page = 5 > newPage ? newPage + 1 : newPage - 1;
        console.log(page , newPage)
        onFetchData( page, newPage );
    }

    return (
      <div className="container mt-5">
        <MaterialTable
            title="Material Table"
            columns={st.columns}
            data={state.data} 
            onRowClick={handleRowClick}
           
            components={{
                Pagination: props => (
                    <TablePagination
                        {...props}
                        rowsPerPageOptions={[ 10, 11]}
                        rowsPerPage={10}
                        count={50}
                        page={state.page}                        
                        onChangePage={( event, newPage) =>                            
                          handleChangePage(newPage)}
                    />
                ),
            }}
        />
        <ShowModal show={show} CloseModal={closeHandler} detail={detail}/>
      </div>
    );
}
