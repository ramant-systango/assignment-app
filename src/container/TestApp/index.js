import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as actions from '../../store/actions/TestApp';
import ShowModal from '../../component/Modal';
import ShowTable from '../../component/Table';

const TestApp = props => {

    const [ show, setShow ] = useState(false);
    const [ detail, setDetail ] = useState({})

    const data = useSelector(state => state.data);
  
    const dispatch = useDispatch();
    const onInitData = useCallback(
        () => dispatch(actions.initData()),
        [dispatch]
    );

    useEffect(
        () => onInitData(),
        [onInitData]
    );

    const onShowModalHandler = (detail) => {
        setShow(true);
        setDetail(detail);
    }
    const closeHandler = () => {
        setShow(false);
    }

    return (  
        <div>
            <ShowTable data={data} onShowModal={onShowModalHandler}  />
            <ShowModal show={show} CloseModal={closeHandler} detail={detail}/>
        </div>      

    );
    
}
 
export default TestApp;