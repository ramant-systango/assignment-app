import axios from 'axios';

const api = axios.create({
    baseURL: 'https://5e904a9a2810f4001648ad5e.mockapi.io/api/v1'
})
export default api;