import React from 'react';
import { Table } from 'react-bootstrap';

const ShowTable = props => {
  const { data, onShowModal } = props
  return (
    <div className="container mt-5">     
    <Table striped bordered hover>
      <thead>
      <tr>
          <th>ID</th>
          <th>Name</th>
          <th>Price</th>
      </tr>
      </thead>
      <tbody >                  
      {(data).map(item => (
      <tr key={item.id} onClick={()=>onShowModal(item)}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price}</td>
      </tr>
      ))}
      </tbody>
    </Table>
    </div>
  );
};

export default ShowTable;