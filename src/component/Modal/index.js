import React  from 'react';
import { Modal, Button } from 'react-bootstrap';

const ShowModal = props => {
    const { show, CloseModal, detail} = props;
    return (
        <>
            <Modal show={show} onHide={CloseModal}>
                <Modal.Header closeButton>
                <Modal.Title>Other Details</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <b>Name : </b>{detail.name}
                    <br />
                    <b>Price : </b>{detail.price}
                    <br />
                    <b>Availability : </b>{detail.availability}
                    <br />
                    <b>Creation Date : </b>{detail.createdAt}
                </Modal.Body>
                <Modal.Footer>
                <Button variant="secondary" onClick={CloseModal}>
                    Close
                </Button>
                </Modal.Footer>
            </Modal>
        </>
    )
}
export default ShowModal